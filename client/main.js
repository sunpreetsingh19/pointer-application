import React from 'react';
import ReactDOM from 'react-dom';
import {Meteor} from 'meteor/meteor';
import {People, calcPosition} from '../imports/api/people';
import {Tracker} from 'meteor/tracker';

import PeopleList from '../imports/ui/PeopleList';

import App from '../imports/ui/App';
/*
Tracker.autorun(function(){
    console.log('List of People',People.find().fetch());
}
);
*/
//hacky thing not okay
/*setTimeout(function(){

},1000);*/

/*

const people= [{
  _id: '1',
  name: 'Sunpreet',
  score: 10
},{
  _id: '2',
  name: 'Navneet',
  score: 30
},{
  _id: '3',
  name: 'Lovish',
  score: -30
}];
*/
//Converting to Arrow Functions
/*const renderPeople= function(peopleList){

return peopleList.map( function(peopleId){
  return <p key={peopleId._id}>{peopleId.name} has scored {peopleId.score} point(s)</p>;
});
}*/



Meteor.startup(() => {
    //call tracker.autorun
    //create variable called people and sset equal to fetch query
    //render people to the screen
    Tracker.autorun(() => {
            let people = People.find({}, {sort:{score: -1}}).fetch();
      let positions = calcPosition(people);
            let title = 'Pointer Application';
        let subtitle= 'created by Sunpreet Singh';
        ReactDOM.render(<App people={positions} title={title} subtitle={subtitle}/>, document.getElementById('root'));
    });
//insert new doc into people collection

});
