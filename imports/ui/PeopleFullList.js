import React from 'react';
import PeopleList from './PeopleList';
import FlipMove from 'react-flip-move';

class PeopleFullList extends React.Component{
    
    renderPeople(){
        if(this.props.people.length===0){
        return (
            <div className="item">
            <p className="item__message">Please Enter the name of First Person</p>
            </div>
        )
        }else{
            

    return this.props.people.map((peopleId) => {
    
     return <PeopleList key={peopleId._id} people={peopleId}/> 
        });
      
 
    }
    }
    
    render(){
        return(
        <div>
            <FlipMove maintainContainerHeight={true}>
            {this.renderPeople()}
            </FlipMove>
            </div>
        );
    }
}

export default PeopleFullList;