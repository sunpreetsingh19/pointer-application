import React from 'react';
import {People} from '../api/people';
//import ReactTimeAgo from 'ReactTimeAgo';
class PeopleList extends React.Component{
    
    //main content
        // button to delete from db
    render(){
        let itemNewClass= `item item--position-${this.props.people.rank}`;
      //  let time= <TimeStamp Lastseen={new Date()}/>;
        return(
       
             <div key = { this.props.people._id } className={itemNewClass}> 
            <div className="person">
            
            <div>
              <h3 className="person__name">{ this.props.people.name  }</h3>
         
            <p className="person__stats">
            
            {this.props.people.position} place- {this.props.people.score} point(s)
        
            </p> 
            </div>
        
        
        
          <div className="person__actions">
             <button className="button button--round" onClick = {
            () => {
                People.update( this.props.people._id , {$inc: {score: 1 }
                });
            }}> +1 < /button> 
            <button className="button button--round" onClick = {
            () => {
                People.update( this.props.people._id, {$inc: { score: -1}
                });
            }} > -1 < /button> 
                <button className="button button--round" onClick = {
            () => {
                People.remove(
                   this.props.people._id
                );
            }
        } > X < /button> 
            </div>
       
            < / div >
           </div>
        );
    }
}
export default PeopleList;