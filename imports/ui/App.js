import React from 'react';
import TitleBar from './TitleBar';
import AddPeople from './AddPeople';
import PeopleFullList from './PeopleFullList';

class App extends React.Component{
    render(){
        return(
          <div>
                <TitleBar title={this.props.title} subtitle={this.props.subtitle}/>
            <div className="bodyCss">
                <PeopleFullList people={this.props.people}/>
                <AddPeople></AddPeople>
                    </div>
                    </div>
        );
        
    }
}
export default App;
