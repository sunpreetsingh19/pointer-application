import React from 'react';

class TitleBar extends React.Component{
    render(){
        return(
            <div className="title-bar">
            <div className="wrapper">
            <h1>{this.props.title}</h1>
            <h5 className="subTitleBar">{this.props.subtitle}</h5>
            </div>
            </div>
        );
    }
}
export default TitleBar;

TitleBar.propTypes ={
title: React.PropTypes.string.isRequired
};