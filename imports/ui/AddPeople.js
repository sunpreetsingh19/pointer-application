import React from 'react';
import {People} from '../api/people';
import ReactTimeAgo from 'react-time-ago';


class AddPeople extends React.Component{
   
    
  
   handleSubmit(e){
         let PeopleName = e.target.PeopleName.value;
       
       
    e.preventDefault();

    if (PeopleName) {
        e.target.PeopleName.value = '';
       
        //insert a person
        People.insert({
            name: PeopleName,
            score: 0,
           
           
        });
    }
   }
    
    render(){
        return(
            <div className="item">
            
        <form className="form" onSubmit = {this.handleSubmit.bind(this)} >
                <input className="form__input" type = "text"
            name = "PeopleName"
            placeholder = "Add a person" / >
                <button className="button button--addPerson" > > < /button> 
                    < /form >
                </div>
        );
    }
}
export default AddPeople;