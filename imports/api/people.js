import {Meteor} from 'meteor/mongo';
import numeral from 'numeral';
// to add delete update data we need constant| got collection people
export const People= new Mongo.Collection('people');

export const calcPosition = (people)=> {
    let rank=1;
    
    return people.map( (person, index) => { 
    if(index!==0 && people[index-1].score> person.score){
            rank++;
       
        }
                     
       return{
                       ...person,
           rank,
                      position: numeral(rank).format('0o'),
           
                       };
    }
);
};